# Systemd utils and units collection.

## Documentation
Those files should be placed in `/lib/systemd/system/` under Debian and derivates and in `/usr/lib/systemd/system` under everything else (tested Arch and Manjaro).
All have low priority in both I/O and CPU time in order to diminish the impact on the normal system usage.

------


### Duperemover
An utility to deduplicate files on supported filesystem (`btrfs` and `xfs`). It's made of 2 files in accordance to `systemd`'s structure for timers.
Since the scanning and hashing can be quite intensive, the `duperemove.service` is configured to use only half of the available cores for each operation.

WARNING: do NOT both deduplicate and defragment a CoW (Copy on Write) filesystem! Those processes work one against the other, deduplication tries to ""link"" together different blocks, even far apart ones, while defragmentation re-orders those blocks contiguously.


### Mega sync
A mega synchronization service. Uses the `megacmd` commands and server to provide synchronization on a user basis.
To use it: login into the mega account with `mega-login` firstly to initiate the session. Once the session has been established, enable the `megasync@USER.service` for the desired user and start the service.
You can then add folders to synchronize with the `mega-sync`, the service start at your login and will keep them synced.
To enable user services to start before login, remember to run `loginctl enable-linger USER`.


### Syncthing
A free p2p synchronization service. Substitutes the default service from the ononimous program to provide synchronization on a user basis.
To use it, enable the service with `syncthing@USER.service`, then add the desired devices and shares by editing the `.config/syncthing/config.xml` file, the using webUI or the `syncthing-gtk` application.


### Flatpak Cache Cleaner
A service and timer to periodically purge the the flatpak cache. It's made of 2 files in accordance to `systemd`'s structure for timers.

